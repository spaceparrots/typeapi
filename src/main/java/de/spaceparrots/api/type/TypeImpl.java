package de.spaceparrots.api.type;

import de.spaceparrots.api.type.exception.PropertyAlreadyDefinedException;
import de.spaceparrots.api.type.exception.TypeConversionFailedException;
import de.spaceparrots.api.type.exception.UnknownPropertyException;

import java.util.HashMap;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * <h1>TypeAPI</h1>
 *
 * @author Julius Korweck
 * @version 21.01.2019
 * @since 21.01.2019
 */
public class TypeImpl<T> implements Type<T>
{

    protected final TypeContainer parent;

    private final String name;
    private final Class<T> clazz;

    private HashMap<Type<?>, Function<?, T>> resolvers;
    private HashMap<String, Function<T, ?>> propertyResolvers;
    private HashMap<String, Type<?>> propertyTypes;

    private T defaultValue;

    protected TypeImpl( TypeContainer parent, String name, Class<T> clazz, T defaultValue )
    {
        this.parent = parent;
        this.name = name;
        this.clazz = clazz;
        this.defaultValue = defaultValue;
        this.resolvers = new HashMap<>();
        this.propertyResolvers = new HashMap<>();
        this.propertyTypes = new HashMap<>();
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public Class<T> javaClass()
    {
        return clazz;
    }

    @Override
    public T defaultValue()
    {
        return defaultValue;
    }

    @Override
    public <Source> boolean canConvert( Type<Source> sourceType )
    {
        return resolvers.containsKey( sourceType );
    }

    @Override
    public <Source> void introduceConverter( Type<Source> type, Function<Source, T> conversion )
    {
        resolvers.put( type, conversion );
    }

    @Override
    public Set<Type<?>> getConvertibleTypes()
    {
        return resolvers.keySet();
    }

    @Override
    public Set<String> knownProperties()
    {
        return propertyResolvers.keySet();
    }

    @Override
    public boolean hasProperty( String property )
    {
        return knownProperties().contains( property );
    }

    @Override
    public Type<?> getPropertyType( String property )
    {
        if ( !hasProperty( property ) )
            throw new UnknownPropertyException( "There is no such property in type \"" + name() + "\": " + property );
        return propertyTypes.getOrDefault( property, null );
    }

    @Override
    public <Property> void introducePropertyReader( String property, Type<Property> propertyType, Function<T, Property> reader )
    {
        if ( hasProperty( property ) )
            throw new PropertyAlreadyDefinedException( "The property \"" + property + "\" is already defined for type \"" + name() + "\"" );
        propertyResolvers.put( property, reader );
        propertyTypes.put( property, propertyType );
    }

    @Override
    public <Property> Property readPropertyAs( T instance, String property, Type<Property> targetPropertyType )
    {
        if ( !hasProperty( property ) ) return null;
        Type propertyType = getPropertyType( property );
        Function<T, ?> function = propertyResolvers.get( property );
        Object result = function.apply( instance );
        Object resolve = targetPropertyType.convert( propertyType, result, x -> false );
        if ( resolve == null ) return null;
        Property res = (Property) resolve;
        return res;
    }

    @Override
    public <Source> T convertDirectly( Type<Source> type, Source source )
    {
        if ( source == null ) source = type.defaultValue();
        if ( source == null ) return defaultValue();
        Function<Source, T> function = (Function<Source, T>) resolvers.getOrDefault( type, t -> null );
        return function.apply( source );
    }

    @Override
    public <Source> T convert( Type<Source> type, Source source, Predicate<T> rememberNewlyFoundPaths )
    {
        Function<Source, T> resolver = DeepConversionAlgorithm.convert( type, this );
        if ( resolver != null )
        {
            T result;
            try
            {
                result = resolver.apply( source );
            }
            catch ( Exception e )
            {
                throw new TypeConversionFailedException(
                        "Conversion from type \"" + type.name() + "\" to type \"" + name() + "\"", e );
            }
            if ( rememberNewlyFoundPaths.test( result ) )
                introduceConverter( type, resolver );
            return result;
        }
        return null;
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( obj instanceof Type )
        {
            Type t = (Type) obj;
            return name.equals( t.name() ) && clazz.equals( t.javaClass() );
        }
        return super.equals( obj );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2019
 *
 ***********************************************************************************************/