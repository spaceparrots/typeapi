package de.spaceparrots.api.type;

import java.util.stream.Stream;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 06.01.2019
 * @since 06.12.2018
 */
public interface TypeContainer
{

    /**
     * Tries to get the type by its name. The type is genericly undefined.
     *
     * @param name the name of the type
     *
     * @return the type instance matching the name or {@code null} if there is none
     */
    Type<?> getType( String name );

    /**
     * Finds the type by its related java class. The type is genericly predefined.
     *
     * @param clazz the java class related to the type
     * @param <T>   the generic type cast
     *
     * @return the type instance representing the java class or {@code null} if there is none
     */
    <T> Type<T> getType( Class<T> clazz );

    /**
     * Creates an unclosed stream containing all types of this container
     *
     * @return a stream containing all known types of the current container instance
     */
    Stream<Type<?>> types();

    /**
     * Checks whether a type exists using its name.
     *
     * @param name the name of the type
     *
     * @return {@code} true, if and only if there is a type by this name
     *
     * @see #getType(String)
     */
    boolean hasType( String name );

    /**
     * Checks whether a type exists using its related java class.
     *
     * @param clazz the java class related to the type
     *
     * @return {@code} true, if and only if there is a type related to this java class
     *
     * @see #getType(Class)
     */
    boolean hasType( Class<?> clazz );

    /**
     * Introduces a new type to the current container.
     * If there is already a type related to the given java class,
     * overwrite the old one by replacing it with the new one.
     * The default value of the newly introduced type will be {@code null}.
     *
     * @param typeClass the java class related to the new type
     * @param typeName  the name of the new type
     * @param <T>       the generic type cast
     *
     * @return a freshly created type related to the given java class and matching the given name
     */
    <T> Type<T> introduceType( Class<T> typeClass, String typeName );

    /**
     * Introduces a new type to the current container.
     * If there is already a type related to the given java class,
     * overwrite the old one by replacing it with the new one.
     *
     * @param typeClass    the java class related to the new type
     * @param typeName     the name of the new type
     * @param defaultValue the default value for the new type
     * @param <T>          the generic type cast
     *
     * @return a freshly created type related to the given java class, matching the given name and containing the default value
     */
    <T> Type<T> introduceType( Class<T> typeClass, String typeName, T defaultValue );

    /**
     * Introduces a new type to the current container.
     * If there is already a type related to the given java class,
     * overwrite the old one by replacing it with the new one.
     *
     * @param customType the custom type to be added to the container
     *
     * @return the added custom type itself
     */
    <X extends Type<T>, T> X introduceCustomType( X customType );

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/