package de.spaceparrots.api.type;

import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 05.12.2018
 * @since 05.12.2018
 */
public interface Type<T>
{

    /**
     * A name representing the type.
     */
    String name();

    /**
     * The {@link Class} represented by this type object.
     */
    Class<T> javaClass();

    /**
     * Defines a default value for this type.
     *
     * @return {@code null} by default
     */
    default T defaultValue()
    {
        return null;
    }

    /**
     * Searches for a matching conversion methods only in the known conversions list.
     *
     * @param sourceType the source type for the converter to be handled
     *
     * @return <code>true</code> if and only if there a matching conversion method
     *
     * @see #getConvertibleTypes()
     */
    <Source> boolean canConvert( Type<Source> sourceType );

    /**
     * Contains all directly convertible types of the current type
     *
     * @return a set of all types that were previously introduced to the current type with a conversion method
     */
    Set<Type<?>> getConvertibleTypes();

    /**
     * Contains all directly contained properties in any object of this type
     *
     * @return a set of all contained properties previously marked to the current type
     */
    Set<String> knownProperties();

    /**
     * Searches for a property by its name
     *
     * @param property the name of the property
     *
     * @return true if and only if there is a property by the given name
     */
    boolean hasProperty( String property );

    /**
     * Get the type of the property
     *
     * @return the type of the property or null if there is no property by the given name
     */
    Type<?> getPropertyType( String property );

    /**
     * Introduces a new reader to the current type for reading a property of an object belonging to the current type
     *
     * @param property     the property name
     * @param propertyType the type of the property
     * @param reader       the method to get the property using an object of the current type
     */
    <Property> void introducePropertyReader( String property, Type<Property> propertyType, Function<T, Property> reader );

    /**
     * Reads a property of an object of the current type to the target type
     *
     * @param instance           an object belonging to the current type
     * @param property           the name of the property
     * @param targetPropertyType the type of the target value to convert the property
     *
     * @return an object of the wanted type according to the property reading and type conversion methods
     */
    <Property> Property readPropertyAs( T instance, String property, Type<Property> targetPropertyType );

    /**
     * Introduces a new converter to the current type for converting an object to the current type
     *
     * @param type       the type of the source
     * @param conversion the conversion function
     * @param <Source>   the source class type
     */
    <Source> void introduceConverter( Type<Source> type, Function<Source, T> conversion );

    /**
     * Directly converts a source to the current type by only using a known conversion method of the current type
     *
     * @param type   the type for finding a matching conversion method
     * @param source the actual instance of a class type matching the given {@link Type}
     *
     * @return an instance representing the source instance using the current {@link Type}
     */
    <Source> T convertDirectly( Type<Source> type, Source source );

    /**
     * Converting a source to the current type by finding one or more converters offering a path
     * from the source type to the current type using a recursive algorithm.<br>
     * The found path can be automatically remembered for future calls by using the predicate indicator to improve performance:<br>
     * e.g: {@code x -> false} as predicate means that the found path to convert the source wont be saved at all;
     * {@code x -> x != null} means that it will be saved if the result of the method will be unequal to null.<br>
     * The predicate will contain the result of the method, before the method returns it,
     * but it is not recommended to use the predicate for any other purposes than the intended one.
     *
     * @param type                    the type for finding a matching conversion method
     * @param source                  the actual instance of a class type matching the given {@link Type}
     * @param rememberNewlyFoundPaths the predicate testing the result for the found path to be added to the conversion method collection
     *
     * @return an instance representing the source instance using the current {@link Type}
     */
    <Source> T convert( Type<Source> type, Source source, Predicate<T> rememberNewlyFoundPaths );

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/