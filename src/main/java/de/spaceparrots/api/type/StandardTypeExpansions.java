package de.spaceparrots.api.type;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 18.12.2018
 * @since 18.12.2018
 */
public final class StandardTypeExpansions
{

    private static void finalizeContainerTypes( TypeContainer c )
    {
        Type<Object> type = c.getType( Object.class );
        c.types().filter( t -> !type.canConvert( t ) ).forEach( t -> type.introduceConverter( t, x -> x ) );
    }

    public static void introduceStandardTypes( TypeContainer c )
    {

        //simple check for preventing double calls
        if ( c.hasType( "string" ) ) return;

        //all primitive types and String
        Type<String> tString = c.introduceType( String.class, "string" );
        Type<Integer> tInteger = c.introduceType( Integer.class, "int", 0 );
        Type<Long> tLong = c.introduceType( Long.class, "long", 0L );
        Type<Double> tDouble = c.introduceType( Double.class, "double", 0D );
        Type<Float> tFloat = c.introduceType( Float.class, "float", 0F );
        Type<Character> tChar = c.introduceType( Character.class, "char" );
        Type<Boolean> tBoolean = c.introduceType( Boolean.class, "boolean", false );
        Type<Short> tShort = c.introduceType( Short.class, "short", (short) 0 );
        Type<Byte> tByte = c.introduceType( Byte.class, "byte", (byte) 0 );
        Type<Void> tVoid = c.introduceType( Void.class, "void" );
        Type<Object> tObject = c.introduceType( Object.class, "object" );
        Type<Type> tType = c.introduceType( Type.class, "type" );
        Type<Class> tClass = c.introduceType( Class.class, "class" );

        //Void should not be resolvable, since there is nothing to be resolved -> null
        //these resolvers are still necessary to prevent beeing filled by the finalize operation adding object resolvers
        tVoid.introduceConverter( tObject, x -> null );
        tObject.introduceConverter( tVoid, x -> null );

        //anything to String
        tString.introduceConverter( tInteger, String::valueOf );
        tString.introduceConverter( tBoolean, String::valueOf );
        tString.introduceConverter( tLong, String::valueOf );
        tString.introduceConverter( tShort, String::valueOf );
        tString.introduceConverter( tDouble, String::valueOf );
        tString.introduceConverter( tFloat, String::valueOf );
        tString.introduceConverter( tByte, String::valueOf );
        tString.introduceConverter( tChar, String::valueOf );

        tString.introduceConverter( tObject, String::valueOf );

        tString.introduceConverter( tType, Type::name );
        tString.introduceConverter( tClass, Class::getName );

        //length property
        tString.introducePropertyReader( "length", tInteger, String::length );

        //type
        tType.introduceConverter( tString, c::getType );
        tType.introducePropertyReader( "name", tString, Type::name );

        //class
        tClass.introduceConverter( tString, x ->
        {
            try
            {
                return Class.forName( x );
            }
            catch ( Exception e )
            {
                return null;
            }
        } );

        //to number functions:

        //digit value or ascii value
        Function<Character, Number> charToNumber = ch -> Character.isDigit( ch ) ? Character.digit( ch, 10 ) : (int) ch;
        Function<Number, Character> numberToChar = num -> (char) num.intValue();
        //simple binary. 1 is true
        Function<Boolean, Number> boolToNumber = b -> b ? 1 : 0;

        //anything to Integer (int)
        tInteger.introduceConverter( tString, Integer::new );
        tInteger.introduceConverter( tBoolean, b -> boolToNumber.apply( b ).intValue() );
        tInteger.introduceConverter( tLong, Number::intValue );
        tInteger.introduceConverter( tShort, Number::intValue );
        tInteger.introduceConverter( tDouble, Number::intValue );
        tInteger.introduceConverter( tFloat, Number::intValue );
        tInteger.introduceConverter( tByte, Number::intValue );
        tInteger.introduceConverter( tChar, ch -> charToNumber.apply( ch ).intValue() );

        //anything to Byte
        tByte.introduceConverter( tString, Byte::new );
        tByte.introduceConverter( tBoolean, b -> boolToNumber.apply( b ).byteValue() );
        tByte.introduceConverter( tLong, Number::byteValue );
        tByte.introduceConverter( tShort, Number::byteValue );
        tByte.introduceConverter( tDouble, Number::byteValue );
        tByte.introduceConverter( tFloat, Number::byteValue );
        tByte.introduceConverter( tInteger, Number::byteValue );
        tByte.introduceConverter( tChar, ch -> charToNumber.apply( ch ).byteValue() );

        //anything to Short
        tShort.introduceConverter( tString, Short::new );
        tShort.introduceConverter( tBoolean, b -> boolToNumber.apply( b ).shortValue() );
        tShort.introduceConverter( tLong, Number::shortValue );
        tShort.introduceConverter( tInteger, Number::shortValue );
        tShort.introduceConverter( tDouble, Number::shortValue );
        tShort.introduceConverter( tFloat, Number::shortValue );
        tShort.introduceConverter( tByte, Number::shortValue );
        tShort.introduceConverter( tChar, ch -> charToNumber.apply( ch ).shortValue() );

        //anything to Character (char)
        tChar.introduceConverter( tBoolean, b -> b ? '1' : '0' );
        tChar.introduceConverter( tLong, numberToChar::apply );
        tChar.introduceConverter( tShort, numberToChar::apply );
        tChar.introduceConverter( tDouble, numberToChar::apply );
        tChar.introduceConverter( tFloat, numberToChar::apply );
        tChar.introduceConverter( tByte, numberToChar::apply );
        tChar.introduceConverter( tInteger, numberToChar::apply );

        //anything to Boolean
        tBoolean.introduceConverter( tString, Boolean::new );
        tBoolean.introduceConverter( tInteger, i -> i == 1 ? true : i == 0 ? false : null );
        tBoolean.introduceConverter( tLong, i -> i == 1 ? true : i == 0 ? false : null );
        tBoolean.introduceConverter( tShort, i -> i == 1 ? true : i == 0 ? false : null );
        tBoolean.introduceConverter( tByte, i -> i == 1 ? true : i == 0 ? false : null );

        //anything to Long
        tLong.introduceConverter( tInteger, Number::longValue );
        tLong.introduceConverter( tBoolean, b -> boolToNumber.apply( b ).longValue() );
        tLong.introduceConverter( tString, Long::new );
        tLong.introduceConverter( tShort, Number::longValue );
        tLong.introduceConverter( tDouble, Number::longValue );
        tLong.introduceConverter( tFloat, Number::longValue );
        tLong.introduceConverter( tByte, Number::longValue );
        tLong.introduceConverter( tChar, ch -> charToNumber.apply( ch ).longValue() );

        //anything to Double
        tDouble.introduceConverter( tInteger, Number::doubleValue );
        tDouble.introduceConverter( tBoolean, b -> boolToNumber.apply( b ).doubleValue() );
        tDouble.introduceConverter( tString, Double::new );
        tDouble.introduceConverter( tShort, Number::doubleValue );
        tDouble.introduceConverter( tLong, Number::doubleValue );
        tDouble.introduceConverter( tFloat, Number::doubleValue );
        tDouble.introduceConverter( tByte, Number::doubleValue );
        tDouble.introduceConverter( tChar, ch -> charToNumber.apply( ch ).doubleValue() );

        //anything to Float
        tFloat.introduceConverter( tInteger, Number::floatValue );
        tFloat.introduceConverter( tBoolean, b -> boolToNumber.apply( b ).floatValue() );
        tFloat.introduceConverter( tString, Float::new );
        tFloat.introduceConverter( tShort, Number::floatValue );
        tFloat.introduceConverter( tLong, Number::floatValue );
        tFloat.introduceConverter( tDouble, Number::floatValue );
        tFloat.introduceConverter( tByte, Number::floatValue );
        tFloat.introduceConverter( tChar, ch -> charToNumber.apply( ch ).floatValue() );

        finalizeContainerTypes( c );

    }

    public static void introduceBasicNetworkTypes( TypeContainer c )
    {

        //class introduction
        Type<InetAddress> inetAddressType = c.introduceType( InetAddress.class, "inetadress" );
        Type<InetSocketAddress> addressType = c.introduceType( InetSocketAddress.class, "inetsocketadress" );

        //property introduction
        Type<String> stringType = c.getType( String.class );
        inetAddressType.introducePropertyReader( "host", stringType, InetAddress::getHostName );
        addressType.introducePropertyReader( "host", stringType, InetSocketAddress::getHostName );

        //type conversion
        inetAddressType.introduceConverter( addressType, InetSocketAddress::getAddress );

        //TODO: more

        finalizeContainerTypes( c );

    }

    public static void introduceFunctionalProperties( TypeContainer c )
    {

        Type<String> tString = c.getType( String.class );
        tString.introducePropertyReader( "uppercase", tString, String::toUpperCase );
        tString.introducePropertyReader( "lowercase", tString, String::toLowerCase );
        tString.introducePropertyReader( "trimmed", tString, String::trim );

        Type<Boolean> tBoolean = c.getType( Boolean.class );
        tString.introducePropertyReader( "isEmpty", tBoolean, String::isEmpty );

        Type<Character> tChar = c.getType( Character.class );
        tString.introducePropertyReader( "firstChar", tChar, s -> s.charAt( 0 ) );

        boolean hasArrays = c.hasType( Type[].class );

        if ( hasArrays )
        {

            Type<char[]> arrayChar = c.getType( char[].class );
            arrayChar.introduceConverter( tString, String::toCharArray );
            tString.introduceConverter( arrayChar, String::new );

            Type<byte[]> bytes = c.getType( byte[].class );
            bytes.introduceConverter( tString, String::getBytes );
            tString.introduceConverter( bytes, String::new );

        }

        if ( c.hasType( Collection.class ) )
        {

            Type<Object> tObject = c.getType( Object.class );

            //dynamic functional properties
            Type<Set> itemset = c.getType( Set.class );
            itemset.introducePropertyReader( "head", tObject, x -> x.iterator().next() );

            Type<List> itemlist = c.getType( List.class );
            itemlist.introducePropertyReader( "head", tObject, x -> x.get( 0 ) );
            itemlist.introducePropertyReader( "tail", tObject, x -> x.get( x.size() - 1 ) );
            itemlist.introducePropertyReader( "cutTail", tObject, x -> x.size() > 0 ? x.subList( 0, x.size() - 1 ) : x );
            itemlist.introducePropertyReader( "cutHead", tObject, x -> x.subList( 1, x.size() ) );

            //iterator integration

            Type<Iterator> iteratorType = c.introduceType( Iterator.class, "iterator" );

            Type<HashSet> itemset_impl = c.getType( HashSet.class );
            Type<LinkedList> linkeditemlist = c.getType( LinkedList.class );
            Type<ArrayList> arrayitemlist = c.getType( ArrayList.class );
            Type<Collection> collectionType = c.getType( Collection.class );

            Type<Boolean> bool = c.getType( Boolean.class );

            iteratorType.introduceConverter( collectionType, Collection::iterator );
            iteratorType.introduceConverter( itemset, Set::iterator );
            iteratorType.introduceConverter( itemset_impl, HashSet::iterator );
            iteratorType.introduceConverter( itemlist, List::iterator );
            iteratorType.introduceConverter( linkeditemlist, LinkedList::iterator );
            iteratorType.introduceConverter( arrayitemlist, ArrayList::iterator );

            collectionType.introducePropertyReader( "iterator", iteratorType, Collection::iterator );
            itemset.introducePropertyReader( "iterator", iteratorType, Set::iterator );
            itemset_impl.introducePropertyReader( "iterator", iteratorType, HashSet::iterator );
            itemlist.introducePropertyReader( "iterator", iteratorType, List::iterator );
            linkeditemlist.introducePropertyReader( "iterator", iteratorType, LinkedList::iterator );
            arrayitemlist.introducePropertyReader( "iterator", iteratorType, ArrayList::iterator );

            iteratorType.introducePropertyReader( "next", c.getType( Object.class ), Iterator::next );
            iteratorType.introducePropertyReader( "hasNext", bool, Iterator::hasNext );

        }

        finalizeContainerTypes( c );

    }

    public static void introduceDynamicTypes( TypeContainer c )
    {

        if ( c.hasType( Collection.class ) ) return;

        //introduce types
        Type<Set> itemset = c.introduceType( Set.class, "itemset" );
        Type<HashSet> itemset_impl = c.introduceType( HashSet.class, "itemset_impl" );
        Type<List> itemlist = c.introduceType( List.class, "itemlist" );
        Type<LinkedList> linkeditemlist = c.introduceType( LinkedList.class, "linkeditemlist" );
        Type<ArrayList> arrayitemlist = c.introduceType( ArrayList.class, "arrayitemlist" );

        Type<Collection> collectionType = c.introduceType( Collection.class, "itemcollection" );

        //introduce properties
        Type<Integer> integer = c.getType( Integer.class );
        Type<Boolean> bool = c.getType( Boolean.class );

        collectionType.introducePropertyReader( "size", integer, Collection::size );
        collectionType.introducePropertyReader( "isEmpty", bool, Collection::isEmpty );

        itemset.introducePropertyReader( "size", integer, Set::size );
        itemset.introducePropertyReader( "isEmpty", bool, Set::isEmpty );

        itemset_impl.introducePropertyReader( "size", integer, HashSet::size );
        itemset_impl.introducePropertyReader( "isEmpty", bool, HashSet::isEmpty );

        itemlist.introducePropertyReader( "size", integer, List::size );
        itemlist.introducePropertyReader( "isEmpty", bool, List::isEmpty );

        linkeditemlist.introducePropertyReader( "size", integer, LinkedList::size );
        linkeditemlist.introducePropertyReader( "isEmpty", bool, LinkedList::isEmpty );

        arrayitemlist.introducePropertyReader( "size", integer, ArrayList::size );
        arrayitemlist.introducePropertyReader( "isEmpty", bool, ArrayList::isEmpty );

        //introduce conversion
        itemset.introduceConverter( itemset_impl, x -> x );
        itemset_impl.introduceConverter( itemlist, HashSet::new );

        itemlist.introduceConverter( linkeditemlist, x -> x );
        itemlist.introduceConverter( arrayitemlist, x -> x );
        linkeditemlist.introduceConverter( collectionType, LinkedList::new );
        arrayitemlist.introduceConverter( collectionType, ArrayList::new );

        collectionType.introduceConverter( itemset, x -> x );
        collectionType.introduceConverter( itemset_impl, x -> x );
        collectionType.introduceConverter( itemlist, x -> x );
        collectionType.introduceConverter( linkeditemlist, x -> x );
        collectionType.introduceConverter( arrayitemlist, x -> x );

        finalizeContainerTypes( c );

    }

    public static void introduceArrayTypes( TypeContainer c )
    {

        if ( c.hasType( Type[].class ) ) return;

        //class introduction
        Type<String[]> tString = c.introduceType( String[].class, "string-array" );

        Type<char[]> tPChar = c.introduceType( char[].class, "pchar-array" );
        Type<Character[]> tChar = c.introduceType( Character[].class, "char-array" );

        tChar.introduceConverter( tPChar, x ->
        {
            Character[] z = new Character[x.length];
            for ( int i = 0; i < z.length; i++ )
            {
                z[i] = x[i];
            }
            return z;
        } );

        Type<int[]> tPInt = c.introduceType( int[].class, "pint-array" );
        Type<Integer[]> tInt = c.introduceType( Integer[].class, "int-array" );
        Type<double[]> tPDouble = c.introduceType( double[].class, "pdouble-array" );
        Type<Double[]> tDouble = c.introduceType( Double[].class, "double-array" );
        Type<boolean[]> tPBoolean = c.introduceType( boolean[].class, "pboolean-array" );
        Type<Boolean[]> tBoolean = c.introduceType( Boolean[].class, "boolean-array" );
        Type<float[]> tPFloat = c.introduceType( float[].class, "pfloat-array" );
        Type<Float[]> tFloat = c.introduceType( Float[].class, "float-array" );
        Type<long[]> tPLong = c.introduceType( long[].class, "plong-array" );
        Type<Long[]> tLong = c.introduceType( Long[].class, "long-array" );
        Type<short[]> tPShort = c.introduceType( short[].class, "pshort-array" );
        Type<Short[]> tShort = c.introduceType( Short[].class, "short-array" );
        Type<byte[]> tPByte = c.introduceType( byte[].class, "pbyte-array" );
        Type<Byte[]> tByte = c.introduceType( Byte[].class, "byte-array" );
        Type<Object[]> tObject = c.introduceType( Object[].class, "object-array" );
        Type<Type[]> tType = c.introduceType( Type[].class, "type-array" );
        Type<Class[]> tClass = c.introduceType( Class[].class, "class-array" );

        //add resolver for any array type to any another array type
        List<Type> types = c.types()
                .filter( t -> t.name().endsWith( "-array" ) )
                .map( t -> (Type) t )
                .collect( Collectors.toList() );

        int x = 0;
        while ( x < types.size() )
        {
            int y = x;
            while ( y < types.size() )
            {
                help_array_array( c, types.get( x ), types.get( y ) );
                y++;
            }
            x++;
        }

        finalizeContainerTypes( c );

    }

    private static <T> Class<T> help_array_type( Class<T[]> array )
    {
        return (Class<T>) array.getComponentType();
    }

    private static <S, T> void help_array_array( TypeContainer c, Type<S[]> typeArr, Type<T[]> typeArr2 )
    {
        Class<S> clazz = help_array_type( typeArr.javaClass() );
        Class<T> clazz2 = help_array_type( typeArr2.javaClass() );
        if ( clazz == null || clazz2 == null ) return; //wrong type caught, prevent from exception
        if(!c.hasType( clazz ) || !c.hasType( clazz2 )) return; //types does not exist
        Type<S> type1 = c.getType( clazz );
        Type<T> type2 = c.getType( clazz2 );
        if ( clazz.isPrimitive() || clazz2.isPrimitive() ) return; //primitve types can't be resolved
        if ( type1.canConvert( type2 ) )
        {
            Function<T[], S[]> func = a ->
            {

                List<S> collect = Arrays.stream( a ).map( b -> type1.convertDirectly( type2, b ) ).collect( Collectors.toList() );
                S[] arr = (S[]) collect.toArray();
                collect.toArray( arr );
                return arr;
            };
            typeArr.introduceConverter( typeArr2, func );
        }
        if ( type2.canConvert( type1 ) )
        {
            Function<S[], T[]> func2 = a ->
            {

                List<T> collect = Arrays.stream( a ).map( b -> type2.convertDirectly( type1, b ) ).collect( Collectors.toList() );
                T[] arr = (T[]) collect.toArray();
                return arr;
            };
            typeArr2.introduceConverter( typeArr, func2 );
        }
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/