package de.spaceparrots.api.type.exception;

/**
 * <h1>TypeAPI</h1>
 *
 * @author Julius Korweck
 * @version 14.09.2019
 * @since 14.09.2019
 */
public class TypeConversionFailedException extends TypeAPIException
{

    public TypeConversionFailedException( String message, Exception exception )
    {
        super( message );
        initCause( exception );
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2019
 *
 ***********************************************************************************************/