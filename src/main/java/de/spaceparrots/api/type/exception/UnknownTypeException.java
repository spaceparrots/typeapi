package de.spaceparrots.api.type.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 11.12.2018
 * @since 11.12.2018
 */
public class UnknownTypeException extends TypeAPIException
{

    public UnknownTypeException( String message )
    {
        super( message );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/