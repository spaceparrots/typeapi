package de.spaceparrots.api.type.exception;

/**
 * <h1>TypeAPI</h1>
 *
 * @author Julius Korweck
 * @version 14.09.2019
 * @since 14.09.2019
 */
public class PropertyAlreadyDefinedException extends TypeAPIException
{

    public PropertyAlreadyDefinedException( String message )
    {
        super( message );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2019
 *
 ***********************************************************************************************/