package de.spaceparrots.api.type.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
class TypeAPIException extends RuntimeException
{

    TypeAPIException( String message )
    {
        super( message );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/