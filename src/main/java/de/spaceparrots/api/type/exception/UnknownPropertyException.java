package de.spaceparrots.api.type.exception;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 15.12.2018
 * @since 15.12.2018
 */
public final class UnknownPropertyException extends TypeAPIException
{

    public UnknownPropertyException( String message )
    {
        super( message );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/