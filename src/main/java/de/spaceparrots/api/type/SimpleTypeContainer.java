package de.spaceparrots.api.type;

import de.spaceparrots.api.type.exception.UnknownTypeException;

import java.util.HashMap;
import java.util.stream.Stream;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 06.12.2018
 * @since 06.12.2018
 */
public final class SimpleTypeContainer implements TypeContainer
{

    private final HashMap<String, Type<?>> typesByNames;
    private final HashMap<Class<?>, Type<?>> typesByClass;

    SimpleTypeContainer()
    {
        this.typesByClass = new HashMap<>();
        this.typesByNames = new HashMap<>();
        //introduce standard types to this container
        StandardTypeExpansions.introduceStandardTypes( this );
        //connect type implementation with interface type
        Type<String> tString = getType( String.class );
        Type<TypeImpl> tTypeImpl = introduceType( TypeImpl.class, "typeImpl" );
        Type<Type> tType = getType( Type.class );
        tType.introduceConverter( tTypeImpl, x -> x );
        tTypeImpl.introduceConverter( tType, x -> (TypeImpl) x );
        tTypeImpl.introducePropertyReader( "name", tString, Type::name );
    }

    @Override
    public Type<?> getType( String name )
    {
        Type<?> type = typesByNames.get( name );
        if ( type == null )
            throw new UnknownTypeException( "There is no type for name: \"" + name + "\"" );
        return type;
    }

    @Override
    public <T> Type<T> getType( Class<T> clazz )
    {
        Type<T> type = (Type<T>) typesByClass.get( clazz );
        if ( type == null )
            throw new UnknownTypeException( "There is no type for: \"" + clazz.toString() + "\"" );
        return type;
    }

    @Override
    public boolean hasType( Class<?> clazz )
    {
        return typesByClass.containsKey( clazz );
    }

    @Override
    public boolean hasType( String name )
    {
        return typesByNames.containsKey( name );
    }

    @Override
    public Stream<Type<?>> types()
    {
        return typesByClass.values().stream();
    }

    @Override
    public <T> Type<T> introduceType( Class<T> typeClass, String typeName )
    {
        Type<T> type = new TypeImpl<>( this, typeName, typeClass, null );
        return introduceCustomType( type );
    }

    @Override
    public <T> Type<T> introduceType( Class<T> typeClass, String typeName, T defaultValue )
    {
        Type<T> type = new TypeImpl<>( this, typeName, typeClass, defaultValue );
        return introduceCustomType( type );
    }

    @Override
    public <X extends Type<T>, T> X introduceCustomType( X customType )
    {
        typesByClass.put( customType.javaClass(), customType );
        typesByNames.put( customType.name(), customType );
        customType.introduceConverter( customType, t -> t );
        return customType;
    }

    /**
     * Creates a new {@link SimpleTypeContainer} instance.
     */
    public static TypeContainer create()
    {
        return new SimpleTypeContainer();
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/