package de.spaceparrots.api.type;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 07.12.2018
 * @since 07.12.2018
 */
public final class DeepConversionAlgorithm
{

    /**
     * Finds a resolving function for Src->Target by using the known resolvers
     *
     * @param src    the source type to resolve
     * @param target the target type to resolve to
     *
     * @return a function accepting a data of the source type producing an instance of the target type by using known resolvers only
     */
    public static <Src, Target> Function<Src, Target> convert( Type<Src> src, Type<Target> target )
    {
        Function<Src, Target> targetFunction = deepConvert( src, target, new HashSet<>() );
        return targetFunction;
    }

    private static <Src, Target> Function<Src, Target> deepConvert( Type<Src> src, Type<Target> target, Set<String> checkedTypes )
    {
        //A -> C
        if ( target.canConvert( src ) )
            return source -> target.convertDirectly( src, source );
        else checkedTypes.add( target.name() );
        //Search for B that A->B and B->C to generate new A->C via B: A->B->C
        for ( Type<?> resolvable : target.getConvertibleTypes() )
        {
            //checkedTypes that doesnt match are ignored
            if ( !checkedTypes.contains( resolvable.name() ) )
            {
                //generate type safe function A->C giving A,B,C
                Function<Src, Target> deepResolve = deepConvert( src, target, resolvable, checkedTypes );
                if ( deepResolve != null ) //if function is found, work is done here
                    return deepResolve;
            }
        }
        return null;
    }

    private static <Src, Target, Resolvable> Function<Src, Target> deepConvert( Type<Src> src, Type<Target> target, Type<Resolvable> resolvable, Set<String> checkedTypes )
    {
        //recursive finding of function A->B if B->C is set and A->B may contain more than one step
        Function<Src, Resolvable> deepResolve = deepConvert( src, resolvable, checkedTypes );
        if ( deepResolve != null ) //if transforming function is found, generate new function using A->B and B->C in combination
            return source -> target.convertDirectly( resolvable, deepResolve.apply( source ) );
        else return null; //no function found to generate the target
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/