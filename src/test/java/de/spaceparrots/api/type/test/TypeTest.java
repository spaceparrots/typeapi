package de.spaceparrots.api.type.test;

import de.spaceparrots.api.type.SimpleTypeContainer;
import de.spaceparrots.api.type.Type;
import de.spaceparrots.api.type.TypeContainer;
import de.spaceparrots.api.type.exception.PropertyAlreadyDefinedException;
import de.spaceparrots.api.type.exception.TypeConversionFailedException;
import de.spaceparrots.api.type.exception.UnknownPropertyException;
import de.spaceparrots.api.type.exception.UnknownTypeException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * <h1>TypeAPI</h1>
 *
 * @author Julius Korweck
 * @version 14.09.2019
 * @since 14.09.2019
 */
public class TypeTest
{

    private TypeContainer container;

    @Before
    public void setup()
    {
        container = SimpleTypeContainer.create();
    }

    @Test(expected = UnknownTypeException.class)
    public void testUnknownTypeException()
    {
        container.getType( TypeTest.class ); //should throw because type does not exist
    }

    @Test(expected = UnknownPropertyException.class)
    public void testUnknownPropertyException()
    {
        Type<String> stringType = container.getType( String.class );
        stringType.getPropertyType( "definitelyNotExistingProperty" ); //should throw because property does not exist
    }

    @Test(expected = PropertyAlreadyDefinedException.class)
    public void testPropertyAlreadyDefinedException()
    {
        Type<String> stringType = container.getType( String.class );
        String propertyName = "thisPropertyWillBeAddedTwice";
        stringType.introducePropertyReader( propertyName, stringType, s -> s );
        stringType.introducePropertyReader( propertyName, stringType, s -> s ); //should throw because propertyName is already defined
    }

    @Test(expected = TypeConversionFailedException.class)
    public void testConversionFailure()
    {
        Type<String> string = container.getType( String.class );
        Type<Double> doubleType = container.getType( Double.class );
        doubleType.convert( string, "ThatsNotADouble", x -> false );
        //this should throw a TypeConversionFailedException because it cannot convert this
    }

    @Test
    public void testDeepResolveCannotFindAnything()
    {
        Type<TypeTest> testClass1 = container.introduceType( TypeTest.class, "testClass1" );
        Type<TypeContainerTest> testClass2 = container.introduceType( TypeContainerTest.class, "testClass2" );
        //converter for those shouldn't be implemented
        Object shouldBeNull = testClass1.convert( testClass2, null, x -> false );
        assertNull( shouldBeNull );
    }

    @Test
    public void testDefaultValue()
    {
        Type<Integer> typeInteger = container.getType( Integer.class );
        assertEquals( (Integer) 0, typeInteger.defaultValue() );

        Type<String> typeString = container.getType( String.class );
        assertNull( typeString.defaultValue() );
    }

    @Test
    public void testEquals()
    {
        Type<String> type = container.getType( String.class );
        Type<Integer> type2 = container.getType( Integer.class );
        Integer sample = 5;
        assertEquals( type, type );
        assertNotEquals( type, type2 );
        assertNotEquals( type, sample );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2019
 *
 ***********************************************************************************************/