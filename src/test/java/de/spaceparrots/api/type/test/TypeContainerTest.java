package de.spaceparrots.api.type.test;

import de.spaceparrots.api.type.SimpleTypeContainer;
import de.spaceparrots.api.type.StandardTypeExpansions;
import de.spaceparrots.api.type.Type;
import de.spaceparrots.api.type.TypeContainer;
import org.junit.Before;
import org.junit.Test;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

/**
 * <h1>CommandAPI Remastered</h1>
 *
 * @author Julius Korweck
 * @version 14.12.2018
 * @since 14.12.2018
 */
public class TypeContainerTest
{

    private TypeContainer container;

    @Before
    public void setup()
    {
        container = SimpleTypeContainer.create();
        StandardTypeExpansions.introduceArrayTypes( container );
        StandardTypeExpansions.introduceBasicNetworkTypes( container );
        StandardTypeExpansions.introduceDynamicTypes( container );
        //functional as last one
        StandardTypeExpansions.introduceFunctionalProperties( container );
    }

    @Test
    public void introduceResolverTest()
    {
        Type<Double> source = container.getType( Double.class );

        Type<List> target = container.getType( List.class );
        Type<String> string = container.getType( String.class );
        target.introduceConverter( string, s -> Arrays.asList( s.split( Pattern.quote( "." ) ) ) );

        List<String> list = target.convert( source, 5.5D, x -> x != null );

        Object[] actuals = new ArrayList<String>()
        {{
            add( "5" );
            add( "5" );
        }}.toArray();
        assertArrayEquals( "TypeContainer failed deep-resolve conversion", list.toArray(), actuals );
    }

    @Test
    public void resolvePropertyTest()
    {
        Type<String> source = container.getType( String.class );
        String s = source.readPropertyAs( "Text", "uppercase", source );
        assertEquals( "Property resolve test failed", "TEXT", s );
    }

    @Test
    public void typeExpansionTest()
    {

        Class[] shouldKnow = new Class[]
                {
                        //standard basics
                        String.class,
                        Boolean.class,
                        Integer.class, Long.class, Short.class, Byte.class, Character.class,
                        Float.class, Double.class,
                        Class.class, Type.class,
                        Void.class, Object.class,
                        //array types
                        // - non-primitive
                        String[].class,
                        Boolean[].class,
                        Integer[].class, Long[].class, Short[].class, Byte[].class, Character[].class,
                        Float[].class, Double[].class,
                        Class[].class, Type[].class,
                        Object[].class,
                        // - primitive
                        boolean[].class,
                        int[].class, long[].class, short[].class, byte[].class, char[].class,
                        float[].class, double[].class,
                        //dynamic types
                        Iterator.class,
                        Collection.class,
                        Set.class, HashSet.class,
                        List.class, LinkedList.class, ArrayList.class,
                        //network
                        InetAddress.class, InetSocketAddress.class
                };

        //check existence
        for ( Class c : shouldKnow )
        {
            assertTrue( "Class \"" + c.getSimpleName() + "\" is missing", container.hasType( c ) );
        }

    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/